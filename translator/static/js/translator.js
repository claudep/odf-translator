function post_action(url, csrf) {
    $.ajax({
        type: "POST",
        url: url,
        data: "csrfmiddlewaretoken="+csrf,
        async: false,
        success: function(data){
            // With this method (instead of reload) the head of the page is displayed (where a message could be read)
            //if (refresh && refresh=='reload')
            window.location = data;
        },
        error: function (xhr, ajaxOptions, thrownError){
            //$("#attr_edit_errors-0").html(xhr.responseText);
            alert("Sorry, an error occurred.");
            return 'error';
        }
    });
    return true;
}

function delete_doc(url, csrf) {
    var answer = confirm("Do you really want to delete this document?");
    if (!answer) { return false; }
    post_action(url, csrf);
}

function reserve_trans(url, csrf) {
    post_action(url, csrf);
    return false;
}

function open_upload(url) {
    window.open(url, "Upload file", 'width=600,height=500');
    return false;
}


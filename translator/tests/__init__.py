# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
import os
import shutil
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.base import File
from django.core.urlresolvers import reverse
from django.test import TestCase

from translator import models

class TranslatorTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('john', 'doe@example.org', 'johnpw')
        self.old_media_root = settings.MEDIA_ROOT
        settings.MEDIA_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), "media")
        if not os.path.exists(settings.MEDIA_ROOT):
            os.mkdir(settings.MEDIA_ROOT)
        models.overwrite_fs.location = os.path.abspath(settings.MEDIA_ROOT)
        self.lang = models.Language.objects.create(code='fr', name='French')

    def tearDown(self):
        # Delete uploaded files
        shutil.rmtree(settings.MEDIA_ROOT)
        settings.MEDIA_ROOT = self.old_media_root

    def _create_series(self):
        self.series = models.Series.objects.create(title="One Series")
        odt_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_with_img.odt")
        self.doc    = models.Document.objects.create(
            series  = self.series,
            title   = "sample_with_img.odt",
            original= File(open(odt_path, 'r'), name="sample_with_img.odt"),
            up_time = datetime.now(),
        )
        self.doc.update_document()

    def test_create_series(self):
        self.client.login(username='john', password='johnpw')
        odt_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_with_img.odt")
        post_data = {
            'first_doc': File(open(odt_path, 'r')),
            'title'    : "Test Series",
        }
        response = self.client.post(reverse('series_new'), post_data)
        self.assertEqual(models.Series.objects.all().count(), 1)
        series = models.Series.objects.all()[0]
        # Test : odt uploaded to media/<series_uuid>
        self.assertTrue(os.path.exists(os.path.join(settings.MEDIA_ROOT, series.uuid, "sample_with_img.odt")))
        # Test pot created in media/<series_uuid>/pot
        self.assertTrue(os.path.exists(os.path.join(settings.MEDIA_ROOT, series.uuid, "pot", "sample_with_img.pot")))
        # Test image dir with image created
        self.assertTrue(os.path.exists(
            os.path.join(settings.MEDIA_ROOT, series.uuid, "pot", "sample_with_img__images", "10000000000001050000017B3D3D441E.png")))

        self.assertEqual(series.document_set.count(), 1)
        doc = series.document_set.all()[0]
        self.assertEqual(doc.total_str, 3)
        self.assertEqual(series.get_absolute_url(), "/series/%s/" % series.uuid)

    def test_add_document(self):
        # Add a second document to an existing series
        self._create_series()
        odt_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_with_img2.odt")
        post_data = {
            'series'  : self.series.pk,
            'original': File(open(odt_path, 'r')),
        }
        response = self.client.post(reverse('document_new', args=[self.series.uuid]), post_data)
        self.assertEqual(self.series.document_set.count(), 2)
        self.assertTrue(os.path.exists(os.path.join(settings.MEDIA_ROOT, self.series.uuid, "sample_with_img2.odt")))
        self.assertTrue(os.path.exists(os.path.join(settings.MEDIA_ROOT, self.series.uuid, "pot", "sample_with_img2.pot")))
        self.assertTrue(os.path.exists(
            os.path.join(settings.MEDIA_ROOT, self.series.uuid, "pot", "sample_with_img2__images", "10000000000001050000017B3D3D441E.png")))

    def test_replace_document(self):
        self._create_series()
        self.assertEqual(self.doc.total_str, 3)
        odt_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_with_img2.odt")
        post_data = {
            'series'  : self.series.pk,
            'up_file': File(open(odt_path, 'r')),
        }
        response = self.client.post(reverse('document_upload', args=[self.series.uuid, self.doc.pk]), post_data, follow=True)
        self.assertContains(response, "<h1>One Series</h1>")
        self.assertEqual(models.Document.objects.get(pk=self.doc.pk).total_str, 4)

    def test_upload_translation(self):
        self._create_series()
        po_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_with_img.fr.po")
        post_data = {'up_file': File(open(po_path, 'r'))}
        response = self.client.post(reverse('translation_upload', args=[self.series.uuid, self.doc.pk, 'fr']), post_data)
        self.assertRedirects(response, reverse('document_detail', args=[self.series.uuid, self.doc.pk, 'fr']))
        trans = self.doc.translation(self.lang)
        # Test statistics
        self.assertEquals((trans.trans, trans.fuzzy, trans.untrans), (2, 0, 1))
        # Test translated odt generated
        self.assertTrue(os.path.exists(os.path.join(settings.MEDIA_ROOT, self.series.uuid, "sample_with_img.fr.odt")))

    def test_upload_translation_with_errors(self):
        """ Test uploading translation that contains xml formatting errors """
        self._create_series()
        po_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sample_wrong.po")
        post_data = {'up_file': File(open(po_path, 'r'))}
        response = self.client.post(reverse('translation_upload', args=[self.series.uuid, self.doc.pk, 'fr']), post_data)
        trans_fr = self.doc.translation(self.lang)
        self.assertTrue(len(trans_fr.errors) > 10)
        self.assertEqual(trans_fr.translated(), None)

    def test_anonymous_access(self):
        self._create_series()
        response = self.client.get(reverse('series_detail', args=[self.series.uuid]))
        self.assertContains(response, "sample_with_img.odt")

    def test_document_image(self):
        self._create_series()
        docimage = self.doc.documentimage_set.all()[0]
        response = self.client.get(reverse('document_image', args=[self.series.uuid, docimage.pk, 'fr']))
        self.assertContains(response, "10000000000001050000017B3D3D441E.png")

    def test_reserve_translation(self):
        self._create_series()
        self.client.login(username='john', password='johnpw')
        response = self.client.post(
            reverse('document_reserve', args=[self.series.uuid, self.doc.pk, 'fr', 'on']))
        self.assertEqual(self.doc.translation(self.lang).reserved_by, self.user)
        response = self.client.get(reverse('document_detail', args=[self.series.uuid, self.doc.pk, 'fr']))
        self.assertContains(response, "You have reserved this document.")
        response = self.client.post(
            reverse('document_reserve', args=[self.series.uuid, self.doc.pk, 'fr', 'off']))
        self.assertEqual(self.doc.translation(self.lang).reserved_by, None)

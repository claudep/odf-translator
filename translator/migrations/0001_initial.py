# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Series'
        db.create_table('translator_series', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uuid', self.gf('translator.fields.UuidField')(auto=True, max_length=36, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('translator', ['Series'])

        # Adding model 'SeriesPerm'
        db.create_table('translator_seriesperm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('obj', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Series'])),
            ('admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('translator', ['SeriesPerm'])

        # Adding model 'Document'
        db.create_table('translator_document', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('series', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Series'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('original', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('up_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('total_str', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('reserved_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
        ))
        db.send_create_signal('translator', ['Document'])

        # Adding model 'Language'
        db.create_table('translator_language', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=6, db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('translator', ['Language'])

        # Adding model 'Translation'
        db.create_table('translator_translation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Document'])),
            ('lang', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Language'])),
            ('up_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('up_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('merged', self.gf('django.db.models.fields.CharField')(max_length=150, null=True)),
            ('trans', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('fuzzy', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('untrans', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal('translator', ['Translation'])

        # Adding model 'DocumentImage'
        db.create_table('translator_documentimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Document'])),
            ('fname', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('translator', ['DocumentImage'])

        # Adding model 'ImageTranslation'
        db.create_table('translator_imagetranslation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.DocumentImage'])),
            ('lang', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['translator.Language'])),
            ('up_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('up_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('translator', ['ImageTranslation'])


    def backwards(self, orm):
        
        # Deleting model 'Series'
        db.delete_table('translator_series')

        # Deleting model 'SeriesPerm'
        db.delete_table('translator_seriesperm')

        # Deleting model 'Document'
        db.delete_table('translator_document')

        # Deleting model 'Language'
        db.delete_table('translator_language')

        # Deleting model 'Translation'
        db.delete_table('translator_translation')

        # Deleting model 'DocumentImage'
        db.delete_table('translator_documentimage')

        # Deleting model 'ImageTranslation'
        db.delete_table('translator_imagetranslation')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'translator.document': {
            'Meta': {'object_name': 'Document'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'reserved_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'}),
            'series': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Series']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'total_str': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'up_time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'translator.documentimage': {
            'Meta': {'object_name': 'DocumentImage'},
            'doc': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Document']"}),
            'fname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'translator.imagetranslation': {
            'Meta': {'object_name': 'ImageTranslation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.DocumentImage']"}),
            'lang': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Language']"}),
            'up_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'up_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'translator.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '6', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'translator.series': {
            'Meta': {'object_name': 'Series'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'uuid': ('translator.fields.UuidField', [], {'auto': 'True', 'max_length': '36', 'blank': 'True'})
        },
        'translator.seriesperm': {
            'Meta': {'object_name': 'SeriesPerm'},
            'admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'obj': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Series']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'translator.translation': {
            'Meta': {'object_name': 'Translation'},
            'doc': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Document']"}),
            'fuzzy': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['translator.Language']"}),
            'merged': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'trans': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'untrans': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'up_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'up_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['translator']

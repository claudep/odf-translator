# -*- coding: utf-8 -*-

# Copyright (C) 2011 Claude Paroz <claude@2xlibre.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls.defaults import patterns, include, url
from django.views.generic import TemplateView

from registration.views import SiteRegister, AccountActivation

urlpatterns = patterns('',
    url(r'^login/$',  'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^register/$', SiteRegister.as_view(), name='register'),
    url(r'^register/success/$',
        TemplateView.as_view(template_name="registration/register_success.html"),
        name='register_success'),
    url(r'^activate/(?P<key>\w+)$$', AccountActivation.as_view(), name='activation'),
)
